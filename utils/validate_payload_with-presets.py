def validate(model,request):
    for i in model['keys']:
        if i not in request:
            if model['keys'][i]['required']:
                return False, f'missing mandatory key {i}'
            else:
                request[i]=model['keys'][i]['preset']
        else:
            if type(request[i])!=model['keys'][i]['type']:
                return False, f"wrong type for {i}, should be {model['keys'][i]['type']}, found {type(request[i])}"
            if 'options' in model['keys'][i] and request[i] not in model['keys'][i]['options']:
                return False, f"wrong value for {i}, should be one of {model['keys'][i]['options']}, found {request[i]}"
    return True, request

data_model={
    "keys":{
        'bla':{
            'required':False,
            'type':str,
            'preset':'100',
            'options':['BV','VOF','EMZ']
        }
    }
}
request={
    'bla':'300'
}

print(validate(data_model,request))