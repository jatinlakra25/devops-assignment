import json, boto3, os

s3 = boto3.client('s3')

BUCKET=os.environ['BUCKET']
FOLDER=os.environ['FOLDER']

all_files_in_bucket = ""

def get_best_fit(event, context):
    body={} if event['body']==None else json.loads(event['body'])
    all_files_in_bucket = [i['Key'] for i in s3.list_objects(Bucket=BUCKET)['Contents']]

    response=validate_request(body,all_files_in_bucket,FOLDER)
    if response['statusCode']!=200: return response
    lf = body['legal_form'] if body['legal_form'] == 'BV' else 'NON_BV'
    ln = 'ASSET_LOAN' if body['asset'] else 'NON_ASSET_LOAN'
    r_product = f'{lf}_{ln}'

    return run_logic(body,f"{FOLDER}{r_product}-{body['risk_class']}.json")

def validate_request(body,files,folder):
    for i in ['risk_class','legal_form','asset','monthly_redemption_capacity','requested_term','requested_size','upsell']:
        if i not in body:
            return return_response(400, {'message':f"key:'{i}' not in request"})
    if body['legal_form'] not in ['BV','EMZ','VOF']:
        return return_response(400,{'message':'legal form should be BV, EMZ or VOF'})
    lf = body['legal_form'] if body['legal_form'] == 'BV' else 'NON_BV'
    ln = 'ASSET_LOAN' if body['asset'] else 'NON_ASSET_LOAN'
    r_product = f'{lf}_{ln}'

    if f"{folder}{r_product}-{body['risk_class']}.json" not in files:
        return return_response(400, {'message': f"{folder}{r_product}-{body['risk_class']}.json not in available {all_files_in_bucket}"})
    return return_response(200,{})

def run_logic(body,key):
    prices = get_dict_from_s3(BUCKET, key)
    best_fit, make_offer = find_best_fit(prices,
                                         str(body['monthly_redemption_capacity']),
                                         str(body['requested_size']),
                                         str(body['requested_term']),
                                         body['upsell'])

    if not make_offer: return return_response(200, {'result': 'no offer'})
    return return_response(200,
                           {'result': best_fit['result'], 'Term': int(best_fit['term']), 'size': int(best_fit['size'])})

def find_best_fit(prices,red_cap,size,term,upsell=False):
    red_cap=int(red_cap)
    # {'dimensions':[scenario['sizes'],scenario['terms']],'values':result, 'scenario':scenario}
    sizes=prices['dimensions'][0]
    terms=prices['dimensions'][1]
    first_month_cost=prices['values'][f'{sizes.index(int(size))},{terms.index(int(term))}']
    # scenrio where redemption capacity is sufficent
    if first_month_cost<=red_cap:
        # upsell
        if upsell:
            for s in sorted(sizes,reverse=True):
                if prices['values'][f'{sizes.index(int(s))},{terms.index(int(term))}']<=red_cap:
                    return {'result': 'upsell', 'size': str(s), 'term': term}, True
        # no upsell but can afford loan
        return {'result': 'no change', 'size': size, 'term': term}, True
    # scenrio where redemption capacity is insufficent
    if first_month_cost>red_cap:
        # start with extending
        sizes = sizes[:sizes.index(int(size))+1]
        best_size=sizes[-1]
        for t in terms:
            first_month_cost = prices['values'][f'{sizes.index(int(best_size))},{terms.index(int(t))}']
            if first_month_cost <= red_cap:
                return {'result': 'extend', 'size': best_size, 'term': str(t)}, True
        # otherwise downsell
        while len(sizes)>1:
            first_month_cost=prices['values'][f'{sizes.index(sizes[-1])},{terms.index(terms[-1])}']
            size=sizes.pop()
            if first_month_cost<=red_cap:
                return {'result': 'downsell','size':size,'term':terms[-1]}, True
    return {'result': 'no offer','size':None,'term':None}, False

def get_dict_from_s3(bucket, key):
    data = s3.get_object(Bucket=bucket, Key=key)
    return json.loads(data['Body'].read())

def return_response(code, val):
    return {
        'statusCode': code,
        'body': json.dumps(val),
        'headers': {'Content-Type': 'application/json'}}
