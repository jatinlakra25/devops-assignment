import json, boto3, os

s3 = boto3.client('s3')

BUCKET=os.environ['BUCKET']
FOLDER=os.environ['FOLDER']

def get_price(event, context):
    body={} if event['body']==None else json.loads(event['body'])

    all_files_in_bucket = get_all_files_from_path(s3, BUCKET, folder=FOLDER)

    risk_class_map           = get_dict_from_s3(BUCKET, most_recent_file(all_files_in_bucket, FOLDER, 'risk_class_mapping')[0])
    product_map              = get_dict_from_s3(BUCKET, most_recent_file(all_files_in_bucket, FOLDER, 'product_mapping')[0])
    pricing_constants        = get_dict_from_s3(BUCKET, most_recent_file(all_files_in_bucket, FOLDER, 'pricing_constants')[0])
    ftp_month                = get_dict_from_s3(BUCKET, most_recent_file(all_files_in_bucket, FOLDER, 'ftp')[0])

    data_model1 = {
        "keys": {
            "legal_form": {
                'required': False,
                'type': str,
                'preset': 'EMZ',
                'list': ['BV', 'VOF', 'EMZ']
            },
            "asset": {
                'required': False,
                'type': bool,
                'preset': False
            },
            "risk_class": {
                'required': False,
                'type': str,
                'preset': 'AVERAGE',
                'list': ['VERY_GOOD', 'GOOD', 'AVERAGE']
            },
            "age_months": {
                'required': False,
                'type': int,
                'preset': 50
            }
        }
    }
    success, body = validate(data_model1, body)
    if not success: return return_response(400, body)
    r_product = "{BV}BV_{ASSET}ASSET_LOAN".format(BV='NON_' if body['legal_form'] != 'BV' else '',
                                                  ASSET='NON_' if not body['asset'] else '')
    data_model2 = {
        "keys": {
            "term": {
                'required': True,
                'type': int,
                'range': (product_map[r_product]["MIN_TERM"],
                          product_map[r_product]["MAX_TERM"])
            },
            "size": {
                'required': True,
                'type': int,
                'range': (product_map[r_product]["MIN_SIZE"],
                          product_map[r_product]["MAX_SIZE"])
            }
        }
    }
    success, body = validate(data_model2, body)
    if not success: return return_response(400, body)

    result= get_price_trace(r_product, body['risk_class'],body['term'],body['size'],
                            ftp_month, pricing_constants, product_map, risk_class_map,
                            body['age_months'] < 24)

    return return_response(200, result)

def return_response(code, val):
    return {
        'statusCode': code,
        'body': json.dumps(val),
        'headers': {'Content-Type': 'application/json'}}


def validate(model,request):
    for i in model['keys']:
        if i not in request:
            if model['keys'][i]['required']:
                return False, f'missing mandatory key {i}'
            elif 'preset' in model['keys'][i]:
                request[i]=model['keys'][i]['preset']
        else:
            if type(request[i])!=model['keys'][i]['type']:
                return False, f"wrong type for {i}, should be {model['keys'][i]['type']}, found {type(request[i])}"
            if 'list' in model['keys'][i] and request[i] not in model['keys'][i]['list']:
                return False, f"wrong value for {i}, should be one of {model['keys'][i]['list']}, found {request[i]}"
            if 'range' in model['keys'][i] and request[i] not in range(model['keys'][i]['range'][0],model['keys'][i]['range'][1]+1):
                return False, f"wrong value for {i}, should be in the range {model['keys'][i]['range']}, found {request[i]}"
    return True, request

def get_all_files_from_path(s3, bucket,folder=None):
    all_files=[i['Key'] for i in s3.list_objects(Bucket=bucket)['Contents']]
    if not folder:
        return [i for i in all_files if '/' not in i]
    else:
        return [i.split(folder)[1] for i in all_files if len(i.split(folder)) > 1]

def most_recent_file(l, f, s):
    dte = sorted([i.split(f'_{s}.json')[0] for i in l if len(i.split(f'_{s}.json')) == 2], reverse=True)[0]
    return (f'{f}{dte}_{s}.json', dte)

def get_dict_from_s3(bucket, key):
    data = s3.get_object(Bucket=bucket, Key=key)
    return json.loads(data['Body'].read())

def get_margin(risk_class, size, loan_type, pricing_constants):
    str0='UNDER_THRESHOLD' if size < 100000 else 'OVER_THRESHOLD'
    str1='ASSET' if loan_type['ASSET'] == 1 else 'NON_ASSET'
    return pricing_constants[f'{str0}_{str1}_{risk_class}']

def get_price_trace(product, risk_class, term, size, ftp_month, pricing_constants, product_map, risk_class_map,startup=False):
    result = {"product": product, "risk class": risk_class, "term": term, "size": size}
    result['startup_conservatism']=pricing_constants['STARTUP_CONSERVATISM'] if startup else 0
    result['ftp'] = ftp_month[str(term)]
    result['operational_costs'] = pricing_constants['OPERATIONAL_COSTS']
    result['capital_cost'] = pricing_constants['CAPITAL_COST']
    result['margin'] = get_margin(risk_class, size, product_map[product], pricing_constants)
    result['pd'] = risk_class_map[risk_class]
    result['ead'] = product_map[product]['EAD']
    result['lgd'] = product_map[product]['LGD']
    result['expected_loss'] = result['pd'] * result['ead'] * result['lgd']
    result['conservative_expected_loss'] = result['expected_loss'] * pricing_constants['MODEL_CONSERVATISM']
    result['lifetime_conservative_expected_loss'] = result['conservative_expected_loss'] * pricing_constants[
        'LIFETIME_CONSERVATISM']
    result['interest_rate'] = round(result['lifetime_conservative_expected_loss']
                                   + result['startup_conservatism']
                                   + result['ftp']
                                   + result['operational_costs']
                                   + result['capital_cost']
                                   + result['margin'], 6)
    result['closing_fee'] = min(pricing_constants['CLOSING_FEE_MAX_AMOUNT'],
                               size * pricing_constants['CLOSING_FEE_FRACTION'])
    result['bail_size'] = max(pricing_constants['BAIL_MIN_AMOUNT'], size * pricing_constants['BAIL_FRACTION']) if \
        product_map[product]['BAIL'] == 1 else 0
    result['first_principal'] = round(size / term, 2)
    result['first_interest'] = round(size * result['interest_rate'] * 32 / 365.0 , 2)
    result['first_repayment'] = round(size / term + size * result['interest_rate'] * 32 / 365.0 ,2)
    return result