import json, boto3, os, sys, xlrd

message = {"foo": "bar"}
s3 = boto3.client('s3')
client = boto3.client('sns', region_name='eu-west-1')

BUCKET=os.environ['BUCKET']
FOLDER=os.environ['FOLDER']
arn=os.environ['SNSTopicARN']

def convert_file(event, context):
    filepath = (event['Records'][0]['s3']['object']['key'])
    filename = filepath.split('/')[1].split('.')[0]
    S3Object=s3.get_object(Bucket=BUCKET, Key=filepath)
    data = S3Object['Body'] 
    with open("/tmp/Workbook.xlsx", 'wb') as fs:
        fs.write(data.read())
    workbook = xlrd.open_workbook("/tmp/Workbook.xlsx")
    worksheet = workbook.sheet_by_name("Sheet1")

    data = []
    keys = [v.value for v in worksheet.row(0)]
    for row_number in range(worksheet.nrows):
        if row_number == 0:
            continue
        row_data = {}
        for col_number, cell in enumerate(worksheet.row(row_number)):
            row_data[keys[col_number]] = cell.value
        data.append(row_data)

    with open("/tmp/Workbook.json", 'w') as json_file:
        json_file.write(json.dumps({'data': data}))
    data = open("/tmp/Workbook.json", 'rb')
    Uploadfilename = (f'price-input/{filename}.json')
    print (Uploadfilename)
    s3.put_object(Bucket=BUCKET, Key=Uploadfilename, Body=data, ACL='public-read')

    response = client.publish(
    TargetArn=arn,
    Message=json.dumps({'default': json.dumps(message)}),
    MessageStructure='json'
    )

